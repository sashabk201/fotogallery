
START TRANSACTION;
CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `password` varchar(120) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `email` varchar(120) NOT NULL,
  `dob` datetime NOT NULL,
  `count_of_uploaded_images` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `ImageCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` nvarchar(250) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `ImageCategory`(`Name`) VALUES
('Аниме'),('Девушки'),('Животные'),('Люди'),('Машины'),('Мужчины'),('Пейзаж'),('Растения');

CREATE TABLE IF NOT EXISTS `Images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`),
  FOREIGN KEY (`category_id`) REFERENCES `ImageCategory` (`id`)
);
COMMIT;
