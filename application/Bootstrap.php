<?php
   /* класс bootstrap должен быть задан в ./application/Bootstrap.php */
   class Bootstrap extends Yaf_Bootstrap_Abstract {
        public function _initConfig(Yaf_Dispatcher $dispatcher) {
            $content = file_get_contents(APPLICATION_PATH."/conf/initial.sql");
            Db::instance()->execute($content);
            $config = new Yaf_Config_Ini(APPLICATION_PATH . "/conf/setings.ini","setings");
            Yaf_Registry::set("salt",$config->salt);
            Yaf_Registry::set("httphost",$config->httphost);
        }
        public function _initPlugin(Yaf_Dispatcher $dispatcher) {
            
        }
   }
?>
