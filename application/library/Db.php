<?php
class Db {
    protected $pdo;
    protected static $instance;

    public function __construct()
    {
        $config = new Yaf_Config_Ini(APPLICATION_PATH . "/conf/db.ini","db");
        $this->pdo = new PDO($config->database->params->dsn,$config->database->params->user,$config->database->params->pass);
    }

    public static function instance(){
        if(self::$instance === null){
            self::$instance = new self;
        }
        return self::$instance;
    }

    //return true or false
    //insert, update
    public function execute($sql){
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute();
    }

    //return mass
    //select
    public function query($sql){
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute();
        if($res !== false){
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt->fetchAll();
        }
        return [];
    }
}
?>