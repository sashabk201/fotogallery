<?php
class Generation {

    public static function generateToken ($secret = null){
        if ($secret === null) $secret = Generation::generateSecret();
        $salt = Yaf_Registry::get("salt");
        return ($salt.":".md5($salt.":".$secret));
    }

    public static function generateSecret($length = 32){
        $chars = '0123456789qwertyuiopasdfghjklzxcvbnm';
        $max = strlen($chars)-1;
        $secret = "";
        for ($i = 0;$i<$length;$i++){
            $secret .= $chars[rand(0,$max)];
        }
        return $secret;
    }

}