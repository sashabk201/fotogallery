<?php
class UserController extends Yaf_Controller_Abstract {

    public function indexAction() {

        if(!isset($_COOKIE["Auth"])){
            Yaf_Controller_Abstract::redirect ( "index.php/Auth" );
            die;
        }
        $images = new ImagesModel();
        $this->_view->images = $images->fetchAll();
        //или
        // $this->getView()->word = "hello world";
    }
}
?>