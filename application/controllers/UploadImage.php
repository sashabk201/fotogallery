<?php
class UploadImageController extends Yaf_Controller_Abstract {

    public $actions = array(
        "upload" => "actions/Upload_action.php",
    );

    public function indexAction() {

        if(!isset($_COOKIE["Auth"])){
            Yaf_Controller_Abstract::redirect ( "index.php/Auth" );
            die;
        }
        $categories = DB::instance()->query("SELECT * FROM ImageCategory");
        $this->_view->categories = $categories;
        $this->_view->token = Generation::generateToken(Yaf_Session::getInstance()->get("secret"));
    }
}
?>