<?php
class IndexController extends Yaf_Controller_Abstract {

    public $actions = array(
        "newset" => "actions/NewestGallery_action.php",
        "random" => "actions/RandomGallery_action.php",
        "even" => "actions/EvenGallery_action.php",
    );

    public function indexAction() {

        if(!isset($_COOKIE["Auth"])){
            Yaf_Controller_Abstract::redirect ( "index.php/Auth" );
            die;
        }
        $images = new ImagesModel();
    	$this->_view->images = $images->fetchAll();
       //или
       // $this->getView()->word = "hello world";
   }
}
?>
