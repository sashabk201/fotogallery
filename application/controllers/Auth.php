<?php
class AuthController extends Yaf_Controller_Abstract {

    public $error ="";

    public $actions = array(
        "login" => "actions/Login_action.php",
        "logout" => "actions/Logout_action.php",
        "registration" => "actions/Registration_action.php",
    );

    public function indexAction() {
        if (isset($_COOKIE["Auth"])){
            Yaf_Controller_Abstract::redirect ( "index.php" );
        }
    }
}
?>