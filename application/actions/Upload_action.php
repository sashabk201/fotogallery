<?php
class UploadAction extends Yaf_Action_Abstract {

    public function execute()
    {
        $req = new Yaf_Request_Http();
        if($req->getPost("csrf") == Generation::generateToken(Yaf_Session::getInstance()->get("secret"))){
            $err = "<h3>Ошибка! Не удалось загрузить файл на сервер!</h3>";
            $good = "<h3>Файл успешно загружен на сервер</h3>";
            if(!empty($req->getFiles('uploadfile')) && !empty($req->getPost("category"))){
                $cat_id = filter_var($req->getPost("category"),FILTER_VALIDATE_INT);
                $im = new ImagesModel();
                $id = $req->getCookie('Auth');
                $uploaddir = 'public/img/';
                $uploadfile = $uploaddir.basename(time().$req->getFiles('uploadfile')['name']);
                if (copy($req->getFiles('uploadfile')['tmp_name'], $uploadfile))
                {
                    //$image = new Imagick($req->getFiles('uploadfile')['tmp_name']);
                    //print_r($image::identifyImage());
                    if($im->UploadImage($uploadfile,$id,$cat_id) == true)
                        $this->_view->mess = $good;
                    else{
                        unlink($uploadfile);
                        $this->_view->mess = $err;
                    }
                }
                else { $this->_view->mess = $err;  }

            }
        }

    }
}
?>