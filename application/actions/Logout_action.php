<?php
class LogoutAction extends Yaf_Action_Abstract
{

    public function execute()
    {
        if (isset($_COOKIE["Auth"])) {
            Yaf_Session::getInstance()->del("secret");
            setcookie("Auth", "-1", time() - 3600, "/");
            Yaf_Controller_Abstract::redirect("index.php");
        }
    }
}
?>