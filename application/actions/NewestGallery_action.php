<?php
class NewsetAction extends Yaf_Action_Abstract {

    public function execute()
    {
        $this->_view->action = "newset";

        $images = new ImagesModel();
        $this->_view->images = $images->fetchNewSet();
    }
}