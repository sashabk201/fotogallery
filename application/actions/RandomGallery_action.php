<?php
class RandomAction extends Yaf_Action_Abstract {

    public function execute()
    {
        $this->_view->action = "random";
        $images = new ImagesModel();
        $this->_view->images = $images->fetchRandom();
    }
}