<?php
class LoginAction extends Yaf_Action_Abstract {

    public function execute()
    {
        $req = new Yaf_Request_Http();
        if(!empty($req->getPost("login")) && !empty($req->getPost("pass"))){
            $login = filter_var($req->getPost("login"),FILTER_SANITIZE_STRING);
            $pass = filter_var($req->getPost("pass"),FILTER_SANITIZE_STRING);
            $user = UserModel::autorise($login);
            if(password_verify($pass,$user['password'])=== true){
                Yaf_Session::getInstance()->start();
                Yaf_Session::getInstance()->set("secret",Generation::generateSecret());
                setcookie("Auth",$user['id'],time()+60*60*3600,"/");
                Yaf_Controller_Abstract::redirect ( "index.php" );
            }
        }
    }
}
?>