<?php
class EvenAction extends Yaf_Action_Abstract {

    public function execute()
    {
        $this->_view->action = "even";

        $images = new ImagesModel();
        $this->_view->images = $images->fetchEven();

    }
}