<?php
class RegistrationAction extends Yaf_Action_Abstract {

    public function execute()
    {
        $req = new Yaf_Request_Http();
        if(!empty($req->getPost("login")) && !empty($req->getPost("pass")) &&
            !empty($req->getPost("first_name")) &&
            !empty($req->getPost("last_name")) && !empty($req->getPost("email"))){
            $login = filter_var($req->getPost("login"),FILTER_SANITIZE_STRING);
            $pass = filter_var($req->getPost("pass"),FILTER_SANITIZE_STRING);
            $first_name = filter_var($req->getPost("first_name"),FILTER_SANITIZE_STRING);
            $last_name = filter_var($req->getPost("last_name"),FILTER_SANITIZE_STRING);
            $email = filter_var( $req->getPost("email"),FILTER_SANITIZE_EMAIL);
            $passwordHash = password_hash($pass, PASSWORD_DEFAULT);
            $res = UserModel::Registration($login,$passwordHash,$first_name,$last_name,$email);
            if($res === true){
                setcookie("Auth","-1",time()-3600,"/");
                $this->_view->error = false;
            }
            else {
                $this->_view->error = true;
                $this->_view->errorMessage = $res;
            }
        }
    }
}
