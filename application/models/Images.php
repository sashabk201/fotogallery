<?php
class ImagesModel extends Model {

    public $table = "Images";

    private $sql = "SELECT im.id, im.image_path, im.user_id, im.category_id, ic.Name, us.username FROM Images as im
JOIN ImageCategory as ic ON im.category_id = ic.id
JOIN Users as us ON im.user_id = us.id";
    public function fetchAll(){
        return $this->pdo->query($this->sql);
    }

    public function fetchNewSet(){
        $sql = $this->sql." ORDER BY im.id desc LIMIT 3";
        return $this->pdo->query($sql);
    }

    public function fetchRandom(){
        $sql = $this->sql." ORDER BY RAND() LIMIT 3";
        return $this->pdo->query($sql);
    }

    public function fetchEven(){
        $sql = $this->sql." WHERE MOD(im.id, 2) = 0";
        return $this->pdo->query($sql);
    }

    public function UploadImage($path,$userId,$cat_id){
        $sql = "INSERT INTO {$this->table} (image_path,user_id,category_id) VALUES ('$path','$userId','$cat_id')";
        if($this->pdo->execute($sql)){
            $sql = "UPDATE Users SET count_of_uploaded_images = (count_of_uploaded_images+1) WHERE id = '$userId'";
            if($this->pdo->execute($sql))
            {return true;}
            else{
                $sql = "DELETE FROM Images WHERE image_path = '$path' AND user_id = '$userId'";
                $this->pdo->execute($sql);
                return false;
            }
        }
        return false;

    }


}